import { Row } from "antd";
import "./App.css";
import Index from "./components/Index";
import NavBar from "./components/NavBar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Formation from "./components/Formation";
import Competences from "./components/Competences";
import Outils from "./components/Outils";
import Valeurs from "./components/Valeurs";
import Contact from "./components/Contact";
import Realisations from "./components/Realisations";

function App() {
  return (
    <>
      <Router>
        <Row className="mb__nav">
          <NavBar />
        </Row>
        <Row justify="center" align="middle">
          <Switch>
            <Route exact path="/" component={Index} />
            <Route exact path="/formation" component={Formation} />
            <Route exact path="/competences" component={Competences} />
            <Route exact path="/outils" component={Outils} />
            <Route exact path="/valeurs" component={Valeurs} />
            <Route exact path="/realisations" component={Realisations} />
            <Route exact path="/contact" component={Contact} />
          </Switch>
        </Row>
      </Router>
    </>
  );
}

export default App;
