import * as React from "react";
import ToolsCard from "./utils/ToolsCard";
import { Col, Row } from "antd";
import PageIntro from "./utils/PageIntro";
import data from "./data/Page.Intro.json";
import outilData from "./data/outil.data.json";

interface IOutilsProps {}

const Outils: React.FunctionComponent<IOutilsProps> = (props) => {
  const splitBy = (size: number, list: Array<any>) => {
    return list.reduce((acc, curr, i, self) => {
      if (!(i % size)) {
        return [...acc, self.slice(i, i + size)];
      }
      return acc;
    }, []);
  };

  return (
    <>
      <Row
        style={{ width: "100%", marginBottom: "5%" }}
        justify="center"
        align="middle"
      >
        <PageIntro {...data[4]}></PageIntro>
      </Row>

      {splitBy(3, outilData).map((_: Array<any>, indx: number) => (
        <Row
        justify="space-between"
          style={{ width: "90%", marginBottom: "2%" }}
          key={indx}
        >
          {_.map(
            (
              item: { title: string; description: string; src: string },
              j: number
            ) => (
              <Col span={7} key={j}>
                <ToolsCard title={item.title} description={item.description}>
                  <img src={`./assets/` + item.src} alt="logo"  style={{width:"100%"}}/>
                </ToolsCard>
              </Col>
            )
          )}
        </Row>
      ))}
    </>
  );
};

export default Outils;
