import { Button, Card, Form, Input, Modal } from "antd";
import * as React from "react";
import "./Contact.css";
import { SendOutlined } from "@ant-design/icons";

interface IContactProps {}

const formLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

const Contact: React.FunctionComponent<IContactProps> = (props) => {
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    console.log(values);
    success();
  };

  function success() {
    Modal.success({
      content: "Email envoyé avec succès",
    });
    form.resetFields();
  }

  return (
    <>
      <Card title="Contact Form" style={{ width: "75%" }}>
        <Form {...formLayout} form={form} name="contact-form" onFinish={onFinish}>
          <Form.Item
            name={["name"]}
            label="Nom"
            rules={[
              {
                required: true,
                message: "Nom est important",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={["email"]}
            label="Email"
            rules={[
              {
                type: "email",
                required: true,
                message: "Email est important",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name={["phone"]} label="Numero de Telephone">
            <Input style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            name={["message"]}
            label="Message"
            rules={[{ required: true, message: "Message est important" }]}
          >
            <Input.TextArea rows={5} />
          </Form.Item>
          <Form.Item wrapperCol={{ ...formLayout.wrapperCol, offset: 6 }}>
            <Button
              type="primary"
              htmlType="submit"
              icon={<SendOutlined />}
              block
            >
              Evoyer
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
};

export default Contact;
