/* eslint-disable no-restricted-globals */
import { Col, Menu } from "antd";
import * as React from "react";
import "./NavBar.css";
import { RouteComponentProps, withRouter } from "react-router-dom";

export interface INavBarProps extends RouteComponentProps {}

class NavBar extends React.Component<INavBarProps> {
  switchPage = (e: any) => {
    this.props.history.push(e.key);
  };

  public render() {
    return (
      <>
        <Col span={2}>
          <button
            className="link-button  logo"
            onClick={() => this.switchPage({ key: "" })}
          >
            <img
              src={"./assets/F.logo.png"}
              alt="Logo"
              style={{ height: 75, width: 75 }}
            />
          </button>
        </Col>

        <Col offset={10} style={{ marginTop: "10px" }}>
          <Menu
            theme="dark"
            mode="horizontal"
            onClick={this.switchPage}
            style={{ background: "#000" }}
          >
            <Menu.Item key="">
              <b>Index</b>
            </Menu.Item>
            <Menu.Item key="formation">
              <b>Formation et Experience</b>
            </Menu.Item>
            <Menu.Item key="competences">
              <b>Compétences</b>
            </Menu.Item>
            <Menu.Item key="outils">
              <b>Outils</b>
            </Menu.Item>
            <Menu.Item key="valeurs">
              <b>Valeurs</b>
            </Menu.Item>
            <Menu.Item key="realisations">
              <b>Realisations</b>
            </Menu.Item>
            <Menu.Item key="contact">
              <b>Contact</b>
            </Menu.Item>
          </Menu>
        </Col>
      </>
    );
  }
}

export default withRouter(NavBar);
