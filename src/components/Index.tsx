import * as React from "react";
import "./Index.css";

export interface IIndexProps {}

export default class Index extends React.Component<IIndexProps> {
  public render() {
    return (
      <>
        <img
          src="./assets/hero.img.png"
          alt="hero profile"
          style={{ height: "80vh", marginTop: "-5%" }}
        />
      </>
    );
  }
}
