import * as React from "react";

interface IValeursProps {}

const Valeurs: React.FunctionComponent<IValeursProps> = (props) => {
  return (
    <>
      <img
        src="/assets/valeurs.pic.png"
        alt="valeurs"
        style={{ marginBottom: "5%" }}
      />
    </>
  );
};

export default Valeurs;
