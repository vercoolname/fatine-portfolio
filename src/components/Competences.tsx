import * as React from "react";
import PageIntro from "./utils/PageIntro";
import data from "./data/Page.Intro.json";

interface ICompetencesProps {}

const Competences: React.FunctionComponent<ICompetencesProps> = (props) => {
  return (
    <>
      <PageIntro {...data[2]}></PageIntro>
    </>
  );
};

export default Competences;
