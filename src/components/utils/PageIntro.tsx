import { Card, Col, Typography } from "antd";
import * as React from "react";

interface IPageIntroProps {
  title: string;
  description: string | string[];
}

const PageIntro: React.FunctionComponent<IPageIntroProps> = (props) => {
  const { Title, Paragraph } = Typography;
  return (
    <>
      <Col span={12}>
        <Card>
          <Typography>
            <Title style={{ marginBottom: "5%", textAlign: "center" }}>
              {props.title}
            </Title>
            {Array.isArray(props.description) ? (
              props.description.map((_, i) => (
                <Paragraph key={i}>
                  <Title level={5}>{_}</Title>
                </Paragraph>
              ))
            ) : (
              <Paragraph>props.description</Paragraph>
            )}
          </Typography>
        </Card>
      </Col>
    </>
  );
};

export default PageIntro;
