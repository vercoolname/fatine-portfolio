import { Card, Col, Row, Typography } from "antd";

import * as React from "react";

interface IToolsCardProps {
  title: string;
  description: string;
}

const ToolsCard: React.FunctionComponent<IToolsCardProps> = (props) => {
  const { Meta } = Card;
  const { Text } = Typography;

  const paragraph = <Text>{props.description}</Text>;

  return (
    <>
      <Card hoverable >
        <Row justify="space-around" align="middle">
          <Col span={6}>{props.children}</Col>
          <Col span={16}>
            <Meta title={props.title} description={paragraph} />
          </Col>
        </Row>
      </Card>
      ,
    </>
  );
};

export default ToolsCard;
