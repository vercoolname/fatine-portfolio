import * as React from "react";
import { Card, Col, Row, Typography, Image, Divider, Tag } from "antd";

interface IFormationCardProps {
  leftToRight?: boolean;
  title?: string;
  description?: string;
  img?: string;
}

const FormationCard: React.FunctionComponent<IFormationCardProps> = (props) => {
  const { Paragraph, Title } = Typography;
  const bordeauxColor = "#960C0C";
  return (
    <>
      <Card style={{ width: "80%" }} hoverable>
        <Row>
          <Col span={16} offset={1}>
            <Typography>
              <Title>Formation</Title>
              <Title level={3}>Ecole Nationale de Commerce et de Gestion</Title>
              <Tag color={bordeauxColor} style={{ marginBottom: "2%" }}>
                2012-2017
              </Tag>
              <Paragraph>
                J’ai passé un cursus de 5 années au sein d’une grande école de
                commerce au Maroc, dans lequel j’ai pu apprendre les bases de la
                macroéconomie, de la comptabilité, des études de marché et du
                marketing pendant les 3 premières années. J’ai pu ensuite me
                spécialiser dans le domaine du commerce international, ou je me
                suis approfondie dans la logistique, l’internationalisation des
                entreprises, le droit commercial international, la fiscalité
                internationale, toujours avec des cours sur le marketing, la
                gestion de la relation client et le management de manière
                générale. J’ai pu ainsi obtenir mon diplôme de grande école en
                commerce international, appuyé d’une expérience sur terrain sous
                forme de stages dans le domaine public, en alliant commerce
                international et diplomatie.
              </Paragraph>
            </Typography>
          </Col>
          <Divider
            type="vertical"
            style={{ height: "300px", marginRight: "2.5%" }}
            plain
          ></Divider>
          <Col
            span={6}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              src={"./assets/formation1.img.jpg"}
              width={320}
              height={250}
              preview={false}
            />
          </Col>
        </Row>
        <Divider
          style={{ borderTop: `2px solid ` + bordeauxColor }}
          plain
        ></Divider>
        <Row>
          <Col
            span={6}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              src={"./assets/formation2.img.jpg"}
              width={320}
              height={250}
              preview={false}
            />
          </Col>
          <Divider
            type="vertical"
            style={{ height: "300px", marginLeft: "2.5%" }}
            plain
          ></Divider>
          <Col span={16} offset={1}>
            <Typography>
              <Title level={3}>
                Institut de Management Public et de Gouvernance Territoriale
              </Title>
              <Tag color={bordeauxColor} style={{ marginBottom: "2%" }}>
                2020-2021
              </Tag>
              <Paragraph>
                M’étant toujours intéressée au marketing et au domaine public,
                j’ai pu allier les deux durant mon 2ème master en Communication
                et Marketing Publics à l’Institut de Management Public et de
                Gouvernance Territoriale à Aix-en-Provence, ou je finalise mon
                cursus universitaire actuellement. Une formation polyvalente qui
                regroupe différents domaines et où j'ai pu recevoir divers cours
                allant de la communication digitale, au marketing social en
                passant par des matières spécialisées dans le marketing
                territorial, l’analyse de données, ou encore le droit de la
                communication et des médias.
              </Paragraph>
            </Typography>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default FormationCard;
