import { Card, Col, Divider, Row, Tag, Typography, Image } from "antd";
import * as React from "react";
import data from "../data/Page.Intro.json";
import experienceData from "../data/experience.data.json";

export interface IExperienceCardProps {}

export default function ExperienceCard(props: IExperienceCardProps) {
  const { Title, Paragraph } = Typography;
  const bordeauxColor = "#960C0C";

  return (
    <div style={{ margin: "5% 10%", width: "80%" }}>
      <Card hoverable>
        <Row justify="center">
          <Col span={5} offset={1}>
            <img
              src="assets/work.experience.svg"
              alt="work svg"
              style={{ width: "250px" }}
            />
          </Col>
          <Col span={16}>
            <Typography>
              <Title style={{ marginBottom: "5%", textAlign: "center" }}>
                {data[1].title}
              </Title>
              {Array.isArray(data[1].description) ? (
                data[1].description.map((_, i) => (
                  <Paragraph key={i}>
                    <Title level={5}>{_}</Title>
                  </Paragraph>
                ))
              ) : (
                <Paragraph>props.description</Paragraph>
              )}
            </Typography>
          </Col>
        </Row>
        <Divider
          style={{ borderTop: `2px solid ` + bordeauxColor, width: "80%" }}
          dashed
        ></Divider>

        {experienceData.map((exp, i) =>
          i % 2 === 0 ? (
            <div key={i}>
              <Row>
                <Col span={16} offset={1}>
                  <Typography>
                    <Title level={3}>{exp.title}</Title>
                    <Tag color={bordeauxColor} style={{ marginBottom: "2%" }}>
                      {exp.date}
                    </Tag>
                    <Paragraph>{exp.description}</Paragraph>
                  </Typography>
                </Col>
                <Divider
                  type="vertical"
                  style={{ height: "300px", marginRight: "2.5%" }}
                  plain
                ></Divider>
                <Col
                  span={6}
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Image
                    src={exp.src}
                    width={320}
                    height={250}
                    preview={false}
                  />
                </Col>
              </Row>
              {i !== experienceData.length && (
                <Divider
                  style={{ borderTop: `2px solid ` + bordeauxColor }}
                  plain
                ></Divider>
              )}
            </div>
          ) : (
            <div key={i}>
              <Row>
                <Col
                  span={6}
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Image
                    src={exp.src}
                    width={320}
                    height={250}
                    preview={false}
                  />
                </Col>
                <Divider
                  type="vertical"
                  style={{ height: "300px", marginLeft: "2.5%" }}
                  plain
                ></Divider>
                <Col span={16} offset={1}>
                  <Typography>
                    <Title level={3}>{exp.title}</Title>
                    <Tag color={bordeauxColor} style={{ marginBottom: "2%" }}>
                      {exp.date}
                    </Tag>
                    <Paragraph>{exp.description}</Paragraph>
                  </Typography>
                </Col>
              </Row>
              {i !== experienceData.length - 1 && (
                <Divider
                  style={{ borderTop: `2px solid ` + bordeauxColor }}
                  plain
                ></Divider>
              )}
            </div>
          )
        )}
      </Card>
    </div>
  );
}
