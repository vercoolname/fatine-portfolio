import * as React from "react";
import ExperienceCard from "./utils/ExperienceCard";
import FormationCard from "./utils/FormationCard";

interface IFormationProps {}

const Formation: React.FunctionComponent<IFormationProps> = (props) => {
  return (
    <>
      <FormationCard></FormationCard>
      <ExperienceCard></ExperienceCard>
    </>
  );
};

export default Formation;
